package Capas;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author MAXIMINO
 */
public  class Escenario extends JPanel{//Se extiende de JPanel para poder usar los dibujos de forma dinamica y no tener que usar interfaces 
   //Posiciones del Pacman
   public int Px=300;
   public int Py=360;
   
   //Angulo inicial de la boca del pacman asi para poder hacer el :V
   public int angulo1=45;
   public int angulo2=280;
   boolean boca=true;//Esta variable es para saber si tiene la boca cerrada o abierta (en este caso cerrada)
   
   //Se crea lo "Visible" de las bolitas en dicha posicion
   public Boolean[] bolitasup = {true,true,true,true,true,true,true,true,true,true,true};//Las de arriba
   public Boolean[] bolitasdw = {true,true,true,true,true,true,true,true,true,true,true};//Las de abajo
   public Boolean[] bolitasct = {true,true,true,true,true,true,true,true,true,true};//Las del centro
   
   public int total=0;//El total del score No le pongo score para variar las cosas :3
   public int total2=32;//Total de bolitas
   
   public int tiempo=300;//Tiempo en que tardara para poder "mover" la boca
   
   public Escenario(){//
        setSize(800,600);//Se Crea el "escenario" o la base que tendra el juego
        setBackground(Color.BLACK);// Se le pone el color negro
       
   }

    public Escenario(int x, int y) {//iguanas
        this.Px = x;
        this.Py = y;
        setSize(800,600);
        setBackground(Color.BLACK);
    }
    
        public int getPx() {
        return Px;
    }

    public void setPx(int Px) {
        this.Px = Px;
    }

    public int getPy() {
        return Py;
    }

    public void setPy(int Py) {
        this.Py = Py;
    }

    public int getAngulo1() {
        return angulo1;
    }

    public void setAngulo1(int angulo1) {
        this.angulo1 = angulo1;
    }

    public int getAngulo2() {
        return angulo2;
    }

    public void setAngulo2(int angulo2) {
        this.angulo2 = angulo2;
    }
    
   //Aqui viene lo bueno comenzar a dibujar
    @Override
    public void paintComponent(Graphics g ){
       super.paintComponent(g);//Se usa el constructor de el metodo para dibujar 
       
       g.setColor (Color.blue);//Se pone color azul al escenario
       
       //Escenario
       //Borde Izquiero(Parte1)
       g.drawLine(0, 200, 50, 200);
       g.drawLine(50, 200, 50, 180);
       g.drawLine(50, 180, 10, 180);
       g.drawLine(10, 180, 10, 10);
       //Borde Izquierdo (Parte2)
       g.drawLine(0, 210, 60, 210);
       g.drawLine(60, 210, 60, 170);
       g.drawLine(60, 170, 15, 170);
       g.drawLine(15, 170, 15, 10);
       
         //Borde Izquiero(Parte3)
       g.drawLine(0, 300, 60, 300);
       g.drawLine(60, 300, 60, 320);
       g.drawLine(60, 320, 15, 320);
       g.drawLine(15, 320, 15, 550);
       //Borde Izquierdo (Parte4)
       g.drawLine(0, 290, 70, 290);
       g.drawLine(70, 290, 70, 335);
       g.drawLine(70, 335, 20, 335);
       g.drawLine(20, 335, 20, 550);
       
       //Borde De Arriba
       g.drawLine(15, 10, 765, 10);
       
       //Borde Derecho (Primera Parte)
       g.drawLine(765, 10, 765, 165);
       g.drawLine(765, 165, 710, 165);
       g.drawLine(710, 165, 710, 200);
       g.drawLine(710, 200, 780, 200);
       
       //Borde Derecho (Parte 2)
       g.drawLine(770,10, 770, 170);
       g.drawLine(770, 170, 720, 170);
       g.drawLine(720, 170, 720, 190);
       g.drawLine(720, 190, 780, 190);
       
       //Borde Derecho (Tercera Parte)
       g.drawLine(760,550,760,335);
       g.drawLine(760,335,710,335);
       g.drawLine(710,335, 710,290);
       g.drawLine(710, 290, 780, 290);
       
       //Borde Derecho (4 Parte)
       g.drawLine(780, 300, 725, 300);
       g.drawLine(725, 300, 725,320);
       g.drawLine(725,320,770,320);
       g.drawLine(770,320,770,550);
       
       //Borde de Abajo
       g.drawLine(20,550,760,550);
       
       //Fin de Los Bordes
       
       //Obstaculos 
       
       //Obstaculo central
        g.drawRect(250,200,300,100);
        g.drawRect(230, 180, 340,150);
       // Obstaculo De Arriba
        g.drawRoundRect( 230, 40, 350, 60, 20, 20 );
       // Obstaculo de Abajo
        g.drawRoundRect( 230, 440, 350, 60, 20, 20 );
        
        //Score
        g.setColor(Color.green);
        g.drawString("Score:"+total,810,20);
       
        
        //Bolitas rojas que se comera pacman
       g.setColor(Color.red);
       //Seccion del obstaculo de arriba
       if(bolitasup[0]==true){
           g.fillArc(255,60,20,20,0,360);
       }
       if(bolitasup[1]==true){
           g.fillArc(285,60,20,20,0,360);
       }
       if(bolitasup[2]==true){
           g.fillArc(315,60,20,20,0,360);
       }
       if(bolitasup[3]==true){
           g.fillArc(345,60,20,20,0,360);
       }
       
       if(bolitasup[4]==true){
           g.fillArc(375,60,20,20,0,360);
       }
       if(bolitasup[5]==true){
           g.fillArc(405,60,20,20,0,360);
       }
       if(bolitasup[6]==true){
           g.fillArc(435,60,20,20,0,360);
       }
       if(bolitasup[7]==true){
           g.fillArc(465,60,20,20,0,360);
       }
       
       if(bolitasup[8]==true){
           g.fillArc(495,60,20,20,0,360);
       }
       if(bolitasup[9]==true){
           g.fillArc(525,60,20,20,0,360);
       }
       if(bolitasup[10]==true){
           g.fillArc(555,60,20,20,0,360);
       }
        //fin de esa seccion
       
       //Seccion del obstaculo de abajo
       if(bolitasdw[0]==true){
           g.fillArc(255,460,20,20,0,360);
       }
       if(bolitasdw[1]==true){
           g.fillArc(285,460,20,20,0,360);
       }
       if(bolitasdw[2]==true){
           g.fillArc(315,460,20,20,0,360);
       }
       if(bolitasdw[3]==true){
           g.fillArc(345,460,20,20,0,360);
       }
       
       if(bolitasdw[4]==true){
           g.fillArc(375,460,20,20,0,360);
       }
       if(bolitasdw[5]==true){
           g.fillArc(405,460,20,20,0,360);
       }
       if(bolitasdw[6]==true){
           g.fillArc(435,460,20,20,0,360);
       }
       if(bolitasdw[7]==true){
           g.fillArc(465,460,20,20,0,360);
       }
       
       if(bolitasdw[8]==true){
           g.fillArc(495,460,20,20,0,360);
       }
       if(bolitasdw[9]==true){
           g.fillArc(525,460,20,20,0,360);
       }
       if(bolitasdw[10]==true){
           g.fillArc(555,460,20,20,0,360);
       }
       
       
       //Bolitas de la Seccion del Centro
       if(bolitasct[0]==true){
           g.fillArc(255,240,20,20,0,360);
       }
       if(bolitasct[1]==true){
           g.fillArc(285,240,20,20,0,360);
       }
       if(bolitasct[2]==true){
           g.fillArc(315,240,20,20,0,360);
       }
       if(bolitasct[3]==true){
           g.fillArc(345,240,20,20,0,360);
       }
       
       if(bolitasct[4]==true){
           g.fillArc(375,240,20,20,0,360);
       }
       if(bolitasct[5]==true){
           g.fillArc(405,240,20,20,0,360);
       }
       if(bolitasct[6]==true){
           g.fillArc(435,240,20,20,0,360);
       }
       if(bolitasct[7]==true){
           g.fillArc(465,240,20,20,0,360);
       }
       
       if(bolitasct[8]==true){
           g.fillArc(495,240,20,20,0,360);
       }
       if(bolitasct[9]==true){
           g.fillArc(525,240,20,20,0,360);
       }/**/
       //Fin de la Seccion de la Derecha
       
       //Fin de la Creacion de las Bolitas
       
       
        //Movimiento/Creacion del pacman
        if(boca){
            g.setColor(Color.yellow);
            g.fillArc(Px, Py, 50, 50, 0, 360);//Tamaño y forma del circulo1 que creara eñ pacman(un circulo redondo que parecera como si cerrara la boca)
            boca=false;
            try {
             Thread.sleep (tiempo);
             repaint();
              } catch (Exception e) {
               }
        }
        else{
        g.setColor(Color.yellow);
        g.fillArc(Px, Py, 50, 50, angulo1, angulo2);//Tamaño y forma del circuloc que creara el pacman(creara una figura como :V)
        boca=true;
        try {
        Thread.sleep (tiempo);
        repaint();
        }
        catch (Exception e) {
        }
        
        }
        //Fin del movimiento/Creacion del Pacman
    }
    
}
