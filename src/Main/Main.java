package Main;

import Capas.Escenario;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author MAXIMINO
 */
/*Se Crea la Clase Main Heredando de Javax.swing.JFrame para poder asi tener la ventana
  y se implementa o se usa "implements" para poder asi obtener los valores de el teclado 
  (en este caso de las teclas de direccion)*/
public class Main extends JFrame implements KeyListener{
    static Escenario base = new Escenario();//Se trae el escenario que sera la base de todo
    static int i;//variable para saber si quiso iniciar partida
    static int f;//Variable para saber si quiso reiniciar la partida
   static  boolean ventana;//Esta variable es para saber si la pantalla esta visible

    public Main(){//Este constructor tiene tiene todo lo que se va a ejecutar despues en el void main(String[] args)
            vista();//Se integra el metodo vista()
        addKeyListener(this);//Se agrega la obcion de poder "oir" los movimientos del teclado esto quiere decir que se puede obtener ese valor siempre y cuando este enfocado
        setFocusable(true);//Con esto se indica que siempre todo el tablero o el juego estara enfocado asi se podra obtener los valores del teclado sobre el jframe
        add(base);//Se agrega el Escenario a la ventana o al Jframe 
        ventana=true;//Con esto indico que la pantalla ya se activo y por ende si esta visible
    }
    
    public void vista(){//Como se eredan los atributes y metodos de JFrame aqui en este metodo uso algunos cmo en el metodo constructor
        setTitle("Ghost PacMan");//Nombre de la ventana o frame
        setSize(910,600);//Tamaño del JFrame
        setVisible(true);// Hacer visible o que se pueda apreciar la ventana
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Que cuando se cierre la ventana se termine el programa
    }
    
     public void reinicio(){//Este metodo se usara cuando se acaben las bolitas en el escenario
         //El JOptionPane.showConfirmDialog() Permite mandar un mensaje para que el usuario acepte o no 
        f=JOptionPane.showConfirmDialog(null, "Felicidades Ganastes ¿Quiere Reiniciar el juego?", "Reinicio de la Partida", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); 
        //Si tocas aceptar el valor que se obtiene es el o si dices cancelar es 1 y si cierras la ventana es-1
        if(f==0){//Si el usuario escoge reiniciar el juego se inicia el proceso para reinicio
        int a=0,b=0;
        this.setVisible(false);/*La ventana actual se cierra para asi poder crear una nueva*/
        new Main();//Se crea todo el entorno otra vez Pero con los datos antiguos de las bolitas y del pacman asi que se debe de reestablecer esos valores otra vez
        while(a<=10){//Se vuelve a "Crear" las bolitas
            base.bolitasup[a]=true;
            base.bolitasdw[a]=true;
            a++;
        }
        while(b<=9){
            base.bolitasct[b]=true;
            b++;
        }
        base.setPx(300);//Se vuelve a colocar el pacman en la pisicion de inicio
        base.setPy(360);
        base.setAngulo1(45);//Con los mismos angulos
        base.setAngulo2(280);
        base.total2=32;//Con el mismo total de bolitas
        repaint();//Se "refresca" el dibujo
        }
        else{//Si el usuario escoge cancelar o simplemente cierra la ventana se acaba el programa
            System.exit(0);
        }
    }
    public static void main(String[] args){//Este metodo no es necesario que lo explique
    //Se ñe pregunta al usuario si realmente quiere comenzar a jugar
    i=JOptionPane.showConfirmDialog(null, "¿Quiere Comenzar el juego?", "Inicio de Partida", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); 
       
       
       if(i==0){//Si realmente quiere jugar
           
    new Main();//Se inicia el juego como es la primera vez se inicia como previamente se dibujo sin problemas 
    
    try{//Este metodo es para la musica de fondo o sound track
      AudioClip ac=Applet.newAudioClip(new URL("file:pacman_chomp.wav"));//Se Crea o se Concigue la musica por asi decir
      while(ventana){//Mientras la ventana este activa se va a escuchar la musica
      ac.loop();//Se ejecuta la musica o se reproduce
      Thread.sleep(10000);//Se hace una pausa antes que se reproduzca otra vez para que asi suene bien 
      }
    }
    catch(Exception e){//Esto es por si hay error al cargar el audio
      System.err.println(e);
    }
    }
    else{//Si no quiere jugar se acaba el programa
        System.exit(0);
    }
    }
   /*Aqui viene lo bueno y sabroso EL TECLADO
    Como se implemento el KeyListener se usan todos los metodos a la de afuerza aunque solo se quieran usar 1 o 2
    */
    @Override
    public void keyPressed(KeyEvent teclado){//Este metodo es para cuando se aprieta una tecla
        if(teclado.VK_UP==teclado.getKeyCode()){//Cuando apriete la tecla de arriba esto pasara se sabe por que se obtiene con getKeyCode() el valor y ya se tiene las teclas predeterminadas con teclado.VK_"la tecla"
          if(base.total2==0){//Se verifica si se acabaron las bolitas
              reinicio();//Se ya se acabaron se ejecuta el metodo reinicio();
          }
          if(base.getPy()<=25){//Si aun hay bolitas en el juego se movera el pacman entonces siempre y cuando no se encuentre mas abajo de 25 py esto queire decir que este dentro del marco del dibujo que dibuje
            base.setPy(25);
          }
          base.setPy(base.getPy()-15);//Cada vez que suba se quitaran 15 posiciones asi podra subir
          base.setAngulo1(125);//y claro como cambia la posicion del pacman tambien debe de cambiar su cara
          base.setAngulo2(300);
      } 
      
      if(teclado.VK_LEFT==teclado.getKeyCode()){//Cuando se toca la tecla derecha
          if(base.total2==0){//Igual que el anterior
              reinicio();
          }
           if(((base.getPx()>=255)&&(base.getPx()<=555))&&((base.getPy()<=80)&&(base.getPy()>=20))){//Esto es para comer las bolitas siempre y cuando el pacman este dentro de este rango de posiciones "x" and "y" 
                  switch(base.getPx()){//Se verifica en que punto x esta para saber que bolita se comera
                      case 255:
                           if(base.bolitasup[0]){
                               base.bolitasup[0]=false;//Dicha bolita estara invisible cuando pase por esa area
                               base.total=base.total+10;//Se supara 10 puntos para el score
                               base.total2=base.total2-1;//Se reduce el numero de bolitas asi para saber cuando reiniciar
                           }
                          break;
                       case 285://Se repite el proceso
                           if(base.bolitasup[1]){
                           base.bolitasup[1]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 315:
                           if(base.bolitasup[2]){
                           base.bolitasup[2]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 345:
                           if(base.bolitasup[3]){
                           base.bolitasup[3]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 375:
                           if(base.bolitasup[4]){
                           base.bolitasup[4]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 405:
                           if(base.bolitasup[5]){
                           base.bolitasup[5]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 435:
                           if(base.bolitasup[6]){
                           base.bolitasup[6]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 465:
                           if(base.bolitasup[7]){
                           base.bolitasup[7]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 495:
                           if(base.bolitasup[8]){
                           base.bolitasup[8]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 525:
                           if(base.bolitasup[9]){
                           base.bolitasup[9]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           if(base.total2==0){
                              reinicio(); 
                           }
                           }
                          break;
                       case 555:
                           if(base.bolitasup[10]){
                           base.bolitasup[10]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           if(base.total2==0){
                              reinicio(); 
                           }
                           }
                          break;
                  }
          }
          if(((base.getPx()>=250)&&(base.getPx()<=535))&&((base.getPy()<=300)&&(base.getPy()>=200))){
                  switch(base.getPx()){
                      case 255:
                           if(base.bolitasct[0]){
                               base.bolitasct[0]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 285:
                           if(base.bolitasct[1]){
                           base.bolitasct[1]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 315:
                           if(base.bolitasct[2]){
                           base.bolitasct[2]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 345:
                           if(base.bolitasct[3]){
                           base.bolitasct[3]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 375:
                           if(base.bolitasct[4]){
                           base.bolitasct[4]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 405:
                           if(base.bolitasct[5]){
                           base.bolitasct[5]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 435:
                           if(base.bolitasct[6]){
                           base.bolitasct[6]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 465:
                           if(base.bolitasct[7]){
                           base.bolitasct[7]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 495:
                           if(base.bolitasct[8]){
                           base.bolitasct[8]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 525:
                           if(base.bolitasct[9]){
                           base.bolitasct[9]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
         
                  }
          }
          if(((base.getPx()>=255)&&(base.getPx()<=555))&&((base.getPy()<=470)&&(base.getPy()>=440))){
                switch(base.getPx()){
                      case 255:
                           if(base.bolitasdw[0]){
                               base.bolitasdw[0]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 285:
                           if(base.bolitasdw[1]){
                           base.bolitasdw[1]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 315:
                           if(base.bolitasdw[2]){
                           base.bolitasdw[2]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 345:
                           if(base.bolitasdw[3]){
                           base.bolitasdw[3]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 375:
                           if(base.bolitasdw[4]){
                           base.bolitasdw[4]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 405:
                           if(base.bolitasdw[5]){
                           base.bolitasdw[5]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 435:
                           if(base.bolitasdw[6]){
                           base.bolitasdw[6]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 465:
                           if(base.bolitasdw[7]){
                           base.bolitasdw[7]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 495:
                           if(base.bolitasdw[8]){
                           base.bolitasdw[8]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 525:
                           if(base.bolitasdw[9]){
                           base.bolitasdw[9]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 555:
                           if(base.bolitasdw[10]){
                           base.bolitasdw[10]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                  }
          }
          if(base.getPx()==0&&((base.getPy()<=280)&&(base.getPy()>=180))){
            base.setPx(780);
          }
          if(base.getPx()<=50&&((base.getPy()>=280)||(base.getPy()<=180))){
            base.setPx(50);
          }
          
          base.setPx(base.getPx()-15);
          base.setAngulo1(230);
          base.setAngulo2(250);
          repaint();
      }
      //En estos 2 metodos se repite lo mismo que los otros 2 :3
      if(teclado.VK_DOWN==teclado.getKeyCode()){
          if(base.total2==0){
              reinicio();
          }
          if(base.getPy()>=480){
            base.setPy(480);
          }
          base.setPy(base.getPy()+15);
          base.setAngulo1(320);
          base.setAngulo2(260);
      }
      
      if(teclado.VK_RIGHT==teclado.getKeyCode()){
          if(base.total2==0){
              reinicio();
          }
          if(((base.getPx()>=255)&&(base.getPx()<=555))&&((base.getPy()<=80)&&(base.getPy()>=20))){
                  switch(base.getPx()){
                      case 255:
                           if(base.bolitasup[0]){
                               base.bolitasup[0]=false;
                               base.total=base.total+10;
                               base.total2=base.total2-1;  
                           }
                          break;
                       case 285:
                           if(base.bolitasup[1]){
                           base.bolitasup[1]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 315:
                           if(base.bolitasup[2]){
                           base.bolitasup[2]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 345:
                           if(base.bolitasup[3]){
                           base.bolitasup[3]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 375:
                           if(base.bolitasup[4]){
                           base.bolitasup[4]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 405:
                           if(base.bolitasup[5]){
                           base.bolitasup[5]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 435:
                           if(base.bolitasup[6]){
                           base.bolitasup[6]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 465:
                           if(base.bolitasup[7]){
                           base.bolitasup[7]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 495:
                           if(base.bolitasup[8]){
                           base.bolitasup[8]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 525:
                           if(base.bolitasup[9]){
                           base.bolitasup[9]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           if(base.total2==0){
                              reinicio(); 
                           }
                           }
                          break;
                       case 555:
                           if(base.bolitasup[10]){
                           base.bolitasup[10]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           if(base.total2==0){
                              reinicio(); 
                           }
                           }
                          break;
                  }
          }
          if(((base.getPx()>=250)&&(base.getPx()<=535))&&((base.getPy()<=300)&&(base.getPy()>=200))){
                  switch(base.getPx()){
                      case 255:
                           if(base.bolitasct[0]){
                               base.bolitasct[0]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 285:
                           if(base.bolitasct[1]){
                           base.bolitasct[1]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 315:
                           if(base.bolitasct[2]){
                           base.bolitasct[2]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 345:
                           if(base.bolitasct[3]){
                           base.bolitasct[3]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 375:
                           if(base.bolitasct[4]){
                           base.bolitasct[4]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 405:
                           if(base.bolitasct[5]){
                           base.bolitasct[5]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 435:
                           if(base.bolitasct[6]){
                           base.bolitasct[6]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 465:
                           if(base.bolitasct[7]){
                           base.bolitasct[7]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 495:
                           if(base.bolitasct[8]){
                           base.bolitasct[8]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 525:
                           if(base.bolitasct[9]){
                           base.bolitasct[9]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
         
                  }
          }
          if(((base.getPx()>=255)&&(base.getPx()<=555))&&((base.getPy()<=470)&&(base.getPy()>=440))){
                switch(base.getPx()){
                      case 255:
                           if(base.bolitasdw[0]){
                               base.bolitasdw[0]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 285:
                           if(base.bolitasdw[1]){
                           base.bolitasdw[1]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 315:
                           if(base.bolitasdw[2]){
                           base.bolitasdw[2]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 345:
                           if(base.bolitasdw[3]){
                           base.bolitasdw[3]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 375:
                           if(base.bolitasdw[4]){
                           base.bolitasdw[4]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 405:
                           if(base.bolitasdw[5]){
                           base.bolitasdw[5]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 435:
                           if(base.bolitasdw[6]){
                           base.bolitasdw[6]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 465:
                           if(base.bolitasdw[7]){
                           base.bolitasdw[7]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 495:
                           if(base.bolitasdw[8]){
                           base.bolitasdw[8]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 525:
                           if(base.bolitasdw[9]){
                           base.bolitasdw[9]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                       case 555:
                           if(base.bolitasdw[10]){
                           base.bolitasdw[10]=false;
                           base.total=base.total+10;
                           base.total2=base.total2-1;
                           }
                          break;
                  }
          }
          if(base.getPx()>=730&&((base.getPy()<=280)&&(base.getPy()>=180))){
            base.setPx(0);
          }
          if(base.getPx()>=700&&((base.getPy()>=280)||(base.getPy()<=180))){
            base.setPx(700);
          }
          base.setPx(base.getPx()+15);
          base.setAngulo1(45);
          base.setAngulo2(280);
          }
    }
   // Este se Ejecuta cuando se toca una tecla especial como f1 o esc o teclas especiales 
    @Override
    public void keyTyped(KeyEvent teclado) {
         //To change body of generated methods, choose Tools | Templates.
    }
    //Este metodo es para cuando se toca y se suelta una tecla 
    @Override
    public void keyReleased(KeyEvent teclado) {//En este caso se usa el Esc para salir del juego
        if(teclado.VK_ESCAPE==teclado.getKeyCode()){
            System.exit(0);
            }
        } 

}
